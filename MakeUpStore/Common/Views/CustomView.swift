//
//  CustomView.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 29/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import UIKit

class CustomView: UIView {

    var nibName: String {
        return ""
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupXIB()
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupXIB()
        setupViews()
    }
    
    func setupViews() {}
    
    private func setupXIB() {
        let view = UINib(nibName: nibName, bundle: nil).instantiate(withOwner: self, options: nil).first as! UIView
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
    }
}
