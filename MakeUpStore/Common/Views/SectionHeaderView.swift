//
//  SectionHeaderView.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 25/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import UIKit

class SectionHeaderView: UIView {
    
    init(withTitle title: String) {
        super.init(frame: .zero)
        setupViews(withTitle: title)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews(withTitle title: String) {
        let label = UILabel()
        label.text = title
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        addSubview(label)
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[v0]-16-|", options: [], metrics: nil, views: ["v0": label]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-8-[v0]|", options: [], metrics: nil, views: ["v0": label]))
    }
}
