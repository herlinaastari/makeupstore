//
//  TrayView.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 19/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import UIKit

protocol TrayViewDelegate: class {
}

class TrayContainerView: CustomView {
    override var nibName: String { return "TrayContainerView" }
    
    @IBOutlet weak var tray: UIView!
    @IBOutlet weak var closeButton: UIButton!
    weak var delegate: TrayViewDelegate?
    
    override func setupViews() {
        tray.layer.cornerRadius = 4
    }
    
    func showModalViewWithAnimation() {
        UIView.animate(
            withDuration: 0.3,
            animations: { [weak self] in
                let translateUp = CGAffineTransform(translationX: 0, y: -250)
                self?.tray.transform = translateUp
            }, completion: { [weak self] _ in
                self?.animateTranslateDown()
            }
        )
    }
    
    func hideModalViewWithAnimation() {
        UIView.animate(
            withDuration: 0.3,
            animations: { [weak self] in
                let translateDown = CGAffineTransform(translationX: 0, y: 250)
                self?.tray.transform = translateDown
            }, completion: { [weak self] _ in
                self?.removeFromSuperview()
            }
        )
    }
    
    private func animateTranslateDown() {
        UIView.animate(
            withDuration: 0.3,
            animations: { [weak self] in
                let translateDown = CGAffineTransform(translationX: 0, y: -230)
                self?.tray.transform = translateDown
            }
        )
    }
    
    @IBAction func handleCloseButtonTouchUpInside(_ sender: UIButton) {
        hideModalViewWithAnimation()
    }
}
