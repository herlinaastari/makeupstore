//
//  CartTabBarView.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 09/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import UIKit

protocol CartTabViewDelegate: class {
    func handleOrderButtonTouchUpInside()
}

class CartTabBarView: CustomView {

    override var nibName: String { return "CartTabBarView" }
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var quantityLabel: UILabel!
    @IBOutlet private weak var orderButton: UIButton!
    
    private var price: Double = 0
    private var quantity = 0
    weak var delegate: CartTabViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        orderButton.layer.cornerRadius = 4
    }
    
    func set(price: Double) {
        self.price += price
        priceLabel.text = "$" + "\(self.price)"
    }
    
    func set(quantity: Int) {
        self.quantity += quantity
        quantityLabel.text = "(" + "\(self.quantity)" + " items" + ")"
    }
    
    func set(buttonTitle: String) {
        orderButton.setTitle(buttonTitle, for: .normal)
    }
    
    @IBAction func handleOrderButtonTouchUpInside(_ sender: UIButton) {
        delegate?.handleOrderButtonTouchUpInside()
    }
}
