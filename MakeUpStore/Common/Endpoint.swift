//
//  Endpoint.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 23/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

enum Endpoint: String, CustomStringConvertible, CaseIterable {
    var description: String { return "" }
    case products = "/products.json"
}
