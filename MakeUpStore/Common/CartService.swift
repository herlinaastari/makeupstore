//
//  CartService.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 15/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Foundation

private enum UserDefaultKey: String {
    case cart = "cart"
    case products = "products"
    case deliveryDetails = "delivery_details"
    case deliveryMethods = "delivery_methods"
    case paymentMethods = "payment_methods"
}

protocol CartServiceProtocol: class {
    func getCart() -> Cart?
    func getDeliveryDetails() -> [DeliveryDetail]?
    func getDeliveryMethods() -> [DeliveryMethod]?
    func getPaymentMethods() -> [PaymentMethod]?
    func getProducts() -> [CartProduct]?
    func addProduct(_ product: CartProduct)
    func updateProductQuantity(_ quantity: Int, to product: CartProduct)
    func deleteProduct(_ product: CartProduct)
    func setDummyDeliveryDetail()
    func setDummyPaymentMethod()
    func setDummyDeliveryMethod()
}

class CartService: CartServiceProtocol {
    static let shared = CartService()
    private let userDefaults: UserDefaults!
    
    init(userDefaults: UserDefaults = UserDefaults.standard) {
        self.userDefaults = userDefaults
    }
    
    func getCart() -> Cart? {
        guard let data = userDefaults.data(forKey: UserDefaultKey.cart.rawValue) else {
            return nil
        }
        
        do {
            let decoder = JSONDecoder()
            let cart = try decoder.decode(Cart.self, from: data)
            return cart
        } catch {
            print("Unable to decode Note (\(error))")
        }
        
        return nil
    }
    
    func getDeliveryDetails() -> [DeliveryDetail]? {
        guard let data = userDefaults.data(forKey: UserDefaultKey.deliveryDetails.rawValue) else {
            return nil
        }
        
        do {
            let decoder = JSONDecoder()
            let deliveryDetails = try decoder.decode([DeliveryDetail].self, from: data)
            return deliveryDetails
        } catch {
            print("Unable to decode products (\(error))")
        }
        
        return nil
    }
    
    func getDeliveryMethods() -> [DeliveryMethod]? {
        guard let data = userDefaults.data(forKey: UserDefaultKey.deliveryMethods.rawValue) else {
            return nil
        }
        
        do {
            let decoder = JSONDecoder()
            let deliveryMethods = try decoder.decode([DeliveryMethod].self, from: data)
            return deliveryMethods
        } catch {
            print("Unable to decode products (\(error))")
        }
        
        return nil
    }
    
    func getPaymentMethods() -> [PaymentMethod]? {
        guard let data = userDefaults.data(forKey: UserDefaultKey.paymentMethods.rawValue) else {
            return nil
        }
        
        do {
            let decoder = JSONDecoder()
            let paymentMethods = try decoder.decode([PaymentMethod].self, from: data)
            return paymentMethods
        } catch {
            print("Unable to decode products (\(error))")
        }
        
        return nil
    }
    
    func getProducts() -> [CartProduct]? {
        guard let data = userDefaults.data(forKey: UserDefaultKey.products.rawValue) else {
            return nil
        }
        
        do {
            let decoder = JSONDecoder()
            let products = try decoder.decode([CartProduct].self, from: data)
            return products
        } catch {
            print("Unable to decode products (\(error))")
        }
        
        return nil
    }
    
    private func setProducts(_ products: [CartProduct]) {
        do {
            let encoder = JSONEncoder()
            let data = try encoder.encode(products)
            userDefaults.set(data, forKey: UserDefaultKey.products.rawValue)
        } catch {
            print("Unable to encode product (\(error))")
        }
    }
    
    func addProduct(_ product: CartProduct) {
        guard var products = getProducts(), !products.isEmpty else {
            let products = [product]
            setProducts(products)
            return
        }
        
        if products.first(where: { $0.id == product.id }) == nil {
            products.append(product)
        } else {
            products = products.map {
                var existingProduct = $0
                if $0.id == product.id {
                    existingProduct.quantity = $0.quantity + product.quantity
                }
                return existingProduct
            }
        }
        
        setProducts(products)
    }
    
    func updateProductQuantity(_ quantity: Int, to product: CartProduct) {
        guard var products = getProducts() else {
            return
        }
        
        products = products.map {
            var existingProduct = $0
            if $0.id == product.id {
                existingProduct.quantity = quantity
            }
            return existingProduct
        }
        
        setProducts(products)
    }
    
    func deleteProduct(_ product: CartProduct) {
        guard var products = getProducts() else {
            return
        }
        
        products.removeAll(where: { $0.id == product.id } )
        setProducts(products)
    }
    
    func setDummyDeliveryDetail() {
        let deliveryDetails = [
            DeliveryDetail(id: 1, name: "Lee", phone: "081234567890", address: "Jalan jalan")
        ]
        do {
            let encoder = JSONEncoder()
            let data = try encoder.encode(deliveryDetails)
            userDefaults.set(data, forKey: UserDefaultKey.deliveryDetails.rawValue)
        } catch {
            print("Unable to encode product (\(error))")
        }
    }
    
    func setDummyPaymentMethod() {
        let paymentMethods = [
            PaymentMethod(id: 1, name: "BCA Virtual Account")
        ]
        do {
            let encoder = JSONEncoder()
            let data = try encoder.encode(paymentMethods)
            userDefaults.set(data, forKey: UserDefaultKey.paymentMethods.rawValue)
        } catch {
            print("Unable to encode product (\(error))")
        }
    }
    
    func setDummyDeliveryMethod() {
        let deliveryMethods = [
            DeliveryMethod(id: 1, name: "JNE", price: "59.000", estimatedDays: "3 days")
        ]
        do {
            let encoder = JSONEncoder()
            let data = try encoder.encode(deliveryMethods)
            userDefaults.set(data, forKey: UserDefaultKey.deliveryMethods.rawValue)
        } catch {
            print("Unable to encode product (\(error))")
        }
    }
}
