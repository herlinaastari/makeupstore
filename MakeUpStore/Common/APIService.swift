//
//  APIService.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 23/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Foundation

protocol APIServiceProtocol: class {
    func fetchProducts(
        from endpoint: Endpoint,
        queryParameters: [String: String]?,
        result: @escaping (Result<[Product], APIServiceError>)
        -> Void
    )
    func fetchLocalProducts(
        from endpoint: Endpoint,
        result: @escaping (Result<[Product], APIServiceError>)
        -> Void
    )
}

class APIService: APIServiceProtocol {
    
    static let shared = APIService()
    private let urlSession: URLSessionProtocol
    private let baseURLString = "http://makeup-api.herokuapp.com/api/v1"
    private let jsonDecoder: JSONDecoder = {
        let jsonDecoder = JSONDecoder()
        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-mm-dd"
        jsonDecoder.dateDecodingStrategy = .formatted(dateFormatter)
        return jsonDecoder
    }()
    
    init(urlSession: URLSessionProtocol = URLSession.shared) {
        self.urlSession = urlSession
    }
    
    private func fetchResourcesFromLocal<T: Decodable>(completion: @escaping (Result<T, APIServiceError>) -> Void) {
        let fileName = "makeup"
        
        if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let values = try self.jsonDecoder.decode(T.self, from: data)
                completion(.success(values))
            } catch {
                completion(.failure(.decodeError))
            }
        }
        return
    }
    
    private func fetchResources<T: Decodable>(
        urlComponents: URLComponents,
        completion: @escaping (Result<T, APIServiceError>
        ) -> Void
    ) {
        guard let url = urlComponents.url else {
            completion(.failure(.invalidEndpoint))
            return
        }
        
        urlSession.dataTask(with: url) { (result) in
            switch result {
            case .success(let (response, data)):
                guard let statusCode = (response as? HTTPURLResponse)?.statusCode,
                    200..<299 ~= statusCode else {
                    completion(.failure(.invalidResponse))
                    return
                }
                
                do {
                    let values = try self.jsonDecoder.decode(T.self, from: data)
                    completion(.success(values))
                } catch {
                    completion(.failure(.decodeError))
                }
            case .failure:
                completion(.failure(.apiError))
            }
        }.resume()
    }
    
    func fetchProducts(
        from endpoint: Endpoint,
        queryParameters: [String: String]? = nil,
        result: @escaping (Result<[Product], APIServiceError>)
        -> Void
    ) {        
        guard let url = URL(string: baseURLString + endpoint.rawValue),
            var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true) else {
            return
        }
        
        if let queryParameters = queryParameters {
            urlComponents.setQueryItems(with: queryParameters)
        }
        
        fetchResources(urlComponents: urlComponents, completion: result)
    }
    
    func fetchLocalProducts(
        from endpoint: Endpoint,
        result: @escaping (Result<[Product], APIServiceError>)
        -> Void
    ) {
        fetchResourcesFromLocal(completion: result)
    }
}
