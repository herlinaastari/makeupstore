//
//  BaseDelegate.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 18/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import UIKit

class BaseDelegate: UIResponder {
    func createHomeNavigationController() -> UINavigationController {
        let builder = HomeBuilder()
        let viewController = builder.createViewController()
        let navigationController = UINavigationController(rootViewController: viewController)
        
        navigationController.tabBarItem = UITabBarItem(
            title: "Home",
            image: UIImage(named: "icon-home"),
            tag: 0
        )
        
        return navigationController
    }
    
    func createBrowseNavigationController() -> UINavigationController {
        let viewController = BrowseViewController()
        let navigationController = UINavigationController(rootViewController: viewController)
        
        navigationController.tabBarItem = UITabBarItem(
            title: "Categories",
            image: UIImage(named: "icon-search"),
            tag: 1
        )
        
        return navigationController
    }
    
    func createCartNavigationController() -> UINavigationController {
        let builder = CartBuilder()
        let viewController = builder.createViewController()
        viewController.hidesBottomBarWhenPushed = false
        let navigationController = UINavigationController(rootViewController: viewController)
        
        navigationController.tabBarItem = UITabBarItem(
            title: "Cart",
            image: UIImage(named: "icon-cart"),
            tag: 1
        )
        
        return navigationController
    }
    
    func createSettingsNavigationController() -> UINavigationController {
        let viewController = UIViewController()
        let navigationController = UINavigationController(rootViewController: viewController)
        
        navigationController.tabBarItem = UITabBarItem(
            title: "Settings",
            image: UIImage(named: "icon-settings"),
            tag: 2
        )
        
        return navigationController
    }
}
