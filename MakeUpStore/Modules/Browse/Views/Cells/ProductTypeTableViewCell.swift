//
//  ProductTypeTableViewCell.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 23/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import UIKit

class ProductTypeTableViewCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    func set(iconImageName: String, title: String) {
        iconImageView.image = UIImage(named: iconImageName)
        label.text = title
    }
    
}
