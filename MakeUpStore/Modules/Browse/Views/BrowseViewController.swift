//
//  BrowseViewController.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 23/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import UIKit

class BrowseViewController: UIViewController {
    @IBOutlet private weak var tableView: UITableView!
    private let cellIdentifier = "ProductTypeTableViewCell"
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = false
        title = "Categories"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    private func setupTableView() {
        tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        tableView.rowHeight = 50
    }
    
    private func pushProductsViewController(with productType: String) {
        let builder = ProductsBuilder()
        let viewController = builder.createViewController(with: productType)
        navigationController?.pushViewController(viewController, animated: true)
    }
}

extension BrowseViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ProductType.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ProductTypeTableViewCell else {
            return UITableViewCell()
        }
        if let iconImageName = ProductType(rawValue: indexPath.row)?.iconImageName,
            let title = ProductType(rawValue: indexPath.row)?.title {
            cell.set(iconImageName: iconImageName, title: title)
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let title = ProductType(rawValue: indexPath.row)?.title.lowercased() else {
            return
        }
        pushProductsViewController(with: title)
    }
}
