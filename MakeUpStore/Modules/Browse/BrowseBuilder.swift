//
//  BrowseBuilder.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 23/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

class BrowseBuilder {
    
    func createViewController() -> BrowseViewController {
        let viewController = BrowseViewController()
        return viewController
    }
    
}
