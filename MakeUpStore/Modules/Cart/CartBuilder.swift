//
//  CartBuilder.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 07/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

class CartBuilder {
    func createViewController() -> CartViewController {
        let viewController = CartViewController()
        let presenter = CartPresenter(viewController: viewController)
        let cartService = CartService.shared
        let interactor = CartInteractor(presenter: presenter, cartService: cartService)
        viewController.interactor = interactor
        viewController.hidesBottomBarWhenPushed = true
        return viewController
    }
}
