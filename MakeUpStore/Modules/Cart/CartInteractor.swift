//
//  CartInteractor.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 07/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Foundation

protocol CartInteractorProtocol: class {
    func setData()
    func updateProductQuantity(_ quantity: Int, to product: CartProduct)
    func deleteProduct(onIndex index: Int)
}

class CartInteractor: CartInteractorProtocol {
    let presenter: CartPresenterProtocol
    let cartService: CartServiceProtocol
    
    init(presenter: CartPresenterProtocol, cartService: CartServiceProtocol) {
        self.presenter = presenter
        self.cartService = cartService
    }
    
    func setData() {
        guard let cartProducts = cartService.getProducts() else { return }
        presenter.showData(cartProducts)
    }
    
    func updateProductQuantity(_ quantity: Int, to product: CartProduct) {
        cartService.updateProductQuantity(quantity, to: product)
        
        guard let updatedCartProducts = cartService.getProducts() else { return }
        presenter.showData(updatedCartProducts)
    }
    
    func deleteProduct(onIndex index: Int) {
        guard let products = cartService.getProducts() else { return }
        
        cartService.deleteProduct(products[index])
        presenter.deleteRow(index)
    }
}
