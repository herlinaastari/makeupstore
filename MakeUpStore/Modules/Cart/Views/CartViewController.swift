//
//  CartViewController.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 07/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import UIKit

protocol CartViewControllerProtocol: class {
    func showData(_ cartProducts: [CartProduct])
    func deleteRow(_ row: Int)
}

class CartViewController: UIViewController {

    var interactor: CartInteractorProtocol!
    private var cartProducts: [CartProduct]?
    private let cellIndentifier = "CheckoutProductTableViewCell"
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cartTabView: CartTabBarView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        title = "Your Cart"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupTableView()
        interactor.setData()
        cartTabView.delegate = self
        cartTabView.set(buttonTitle: "Checkout")
    }
    
    private func setupTableView() {
        registerTableViewCell()
        
        tableView.tableFooterView = UIView()
    }
    
    private func registerTableViewCell() {
        tableView.register(UINib(nibName: cellIndentifier, bundle: nil), forCellReuseIdentifier: cellIndentifier)
    }
    
    private func pushCheckoutViewController(with cartProducts: [CartProduct]) {
        let builder = CheckoutBuilder()
        let viewController = builder.createViewController(with: cartProducts)
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    private func showAlert() {
        let alertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        let alertController = UIAlertController(title: "Warning", message: "Please select at least one item", preferredStyle: .alert)
        alertController.addAction(alertAction)
        navigationController?.present(alertController, animated: true, completion: nil)
    }
    
    private func setTotalPrice(product: CartProduct, isSelected: Bool) {
        var newPrice = (product.price * Double(product.quantity))
        if !isSelected {
            newPrice.negate()
        }
        cartTabView.set(price: newPrice)
    }
    
    private func setQuantity(quantity: Int, isSelected: Bool) {
        var newQuantity = quantity
        if !isSelected {
            newQuantity.negate()
        }
        cartTabView.set(quantity: newQuantity)
    }
    
    private func popViewControllerIfEmpty() {
        guard cartProducts?.isEmpty ?? true else { return }
        navigationController?.popViewController(animated: true)
    }
}

extension CartViewController: CartViewControllerProtocol {
    func showData(_ cartProducts: [CartProduct]) {
        self.cartProducts = cartProducts
        tableView.reloadData()
    }
    
    func deleteRow(_ row: Int) {
        cartProducts?.remove(at: row)
        tableView.deleteRows(at: [IndexPath(row: row, section: 0)], with: .automatic)
        
        popViewControllerIfEmpty()
    }
}

extension CartViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartProducts?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIndentifier, for: indexPath) as? CheckoutProductTableViewCell else {
            return UITableViewCell()
        }
        if let cartProduct = cartProducts?[indexPath.row] {
            cell.set(cartProduct)
        }
        cell.selectionStyle = .none
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            interactor.deleteProduct(onIndex: indexPath.row)
        }
    }
}

extension CartViewController: CartTabViewDelegate {
    func handleOrderButtonTouchUpInside() {
        guard let cartProducts = cartProducts else { return }
        var selectedProducts = [CartProduct]()
        for (index, cartProduct) in cartProducts.enumerated() {
            let indexPath = IndexPath(row: index, section: 0)
            let cell = tableView.cellForRow(at: indexPath) as? CheckoutProductTableViewCell
            if cell?.selectedButton.isSelected == true {
                selectedProducts.append(cartProduct)
            }
        }
        
        guard !selectedProducts.isEmpty else {
            showAlert()
            return
        }
        pushCheckoutViewController(with: selectedProducts)
    }
}

extension CartViewController: CheckoutProductTableViewCellDelegate {
    func handleSelectedButtonTouchUpInside(product: CartProduct, isSelected: Bool) {
        setTotalPrice(product: product, isSelected: isSelected)
        setQuantity(quantity: product.quantity, isSelected: isSelected)
    }
    
    func handleMinusButtonTouchUpInside(product: CartProduct, quantity: Int) {
        interactor.updateProductQuantity(quantity, to: product)
    }
    
    func handlePlusButtonTouchUpInside(product: CartProduct, quantity: Int) {
        interactor.updateProductQuantity(quantity, to: product)
    }
}
