//
//  CartPresenter.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 07/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

protocol CartPresenterProtocol: class {
    func showData(_ cartProducts: [CartProduct])
    func deleteRow(_ index: Int)
}

class CartPresenter: CartPresenterProtocol {
    weak var viewController: CartViewControllerProtocol?
    
    init(viewController: CartViewControllerProtocol) {
        self.viewController = viewController
    }
    
    func showData(_ cartProducts: [CartProduct]) {
        viewController?.showData(cartProducts)
    }

    func deleteRow(_ index: Int) {
        viewController?.deleteRow(index)
    }
}
