//
//  HomeInteractor.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 25/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Foundation

protocol HomeInteractorProtocol: class {
    func fetchLocalProducts()
}

class HomeInteractor: HomeInteractorProtocol {
    let presenter: HomePresenterProtocol
    let apiService: APIServiceProtocol
    
    init(presenter: HomePresenterProtocol, apiService: APIServiceProtocol) {
        self.presenter = presenter
        self.apiService = apiService
    }
    
    func fetchLocalProducts() {
        apiService.fetchLocalProducts(from: .products) { (result: Result<[Product], APIServiceError>) in
            switch result {
            case .success(let products):
                let filteredLipstickProducts = products.filter { $0.price != "0.0" && $0.imageLink != nil && $0.productType == "eyeliner" }
                self.presenter.showBestSellerProducts(filteredLipstickProducts)
                
                let filteredProducts = products.filter { $0.price != "0.0" && $0.imageLink != nil && $0.brand == "nyx" }
                self.presenter.showRecommendedProducts(filteredProducts)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}
