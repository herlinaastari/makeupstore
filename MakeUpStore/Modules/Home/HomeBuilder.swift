//
//  HomeBuilder.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 25/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Foundation

class HomeBuilder {
    func createViewController() -> HomeViewController {
        let viewController = HomeViewController()
        let presenter = HomePresenter(viewController: viewController)
        let apiService = APIService.shared
        let interactor = HomeInteractor(presenter: presenter, apiService: apiService)
        viewController.interactor = interactor
        return viewController
    }
    
}
