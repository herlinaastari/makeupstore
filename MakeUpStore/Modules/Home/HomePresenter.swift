//
//  HomePresenter.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 25/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Foundation

protocol HomePresenterProtocol: class {
    func showBestSellerProducts(_ products: [Product])
    func showRecommendedProducts(_ products: [Product])
}

class HomePresenter: HomePresenterProtocol {
    weak var viewController: HomeViewControllerProtocol?
    
    init(viewController: HomeViewControllerProtocol) {
        self.viewController = viewController
    }
    
    func showBestSellerProducts(_ products: [Product]) {
        viewController?.showNewArrivalProducts(products)
    }
    
    func showRecommendedProducts(_ products: [Product]) {
        viewController?.showBestSellersProducts(products)
    }
}
