//
//  HomeViewController.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 25/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import UIKit

protocol HomeViewControllerProtocol: class {
    func showNewArrivalProducts(_ products: [Product])
    func showBestSellersProducts(_ products: [Product])
}

class HomeViewController: UIViewController, UISearchControllerDelegate, UISearchBarDelegate {
    @IBOutlet weak var findYourStuffButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    private let homeCellIdentifier = "HomeTableViewCell"
    private let imageCellIdentifier = "ImageTableViewCell"
    private var newArrivalProducts: [Product]?
    private var recommendedProducts: [Product]?
    private let searchController: UISearchController = UISearchController(searchResultsController:  nil)
    var interactor: HomeInteractorProtocol!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addSearchNavigationbar()
    }
    
    func addSearchNavigationbar() {
        searchController.delegate = self
        searchController.searchBar.delegate = self
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = true
        navigationItem.titleView = searchController.searchBar
        definesPresentationContext = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        interactor.fetchLocalProducts()
    }
    
    private func setupTableView() {
        tableView.register(UINib(nibName: homeCellIdentifier, bundle: nil), forCellReuseIdentifier: homeCellIdentifier)
        tableView.register(UINib(nibName: imageCellIdentifier, bundle: nil), forCellReuseIdentifier: imageCellIdentifier)
        tableView.tableFooterView = UIView()
    }
    
    private func pushProductsViewController(with productType: String) {
        let builder = ProductsBuilder()
        let viewController = builder.createViewController(with: productType)
        navigationController?.pushViewController(viewController, animated: true)
    }
}

extension HomeViewController: HomeViewControllerProtocol {
    func showNewArrivalProducts(_ products: [Product]) {
        self.newArrivalProducts = products
    }
    
    func showBestSellersProducts(_ products: [Product]) {
        self.recommendedProducts = products
    }
}

extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 250
        case 1, 2:
            return (UIScreen.main.bounds.width - 16) / 2 + 16
        default: break
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 0
        case 1, 2:
            return 28
        default: break
        }
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0:
            return nil
        case 1:
            let view = SectionHeaderView(withTitle: "New Arrival")
            return view
        case 2:
            let view = SectionHeaderView(withTitle: "Best Sellers")
            return view
        default:
            break
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: imageCellIdentifier, for: indexPath) as? ImageTableViewCell else {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: homeCellIdentifier, for: indexPath) as? HomeTableViewCell else {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            if let bestSellerProducts = newArrivalProducts {
                cell.reloadData(with: bestSellerProducts)
            }
            cell.delegate = self
            return cell
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: homeCellIdentifier, for: indexPath) as? HomeTableViewCell else {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            if let recommendedProducts = recommendedProducts {
                cell.reloadData(with: recommendedProducts)
            }
            cell.delegate = self
            return cell
        default:
            break
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 1, 2:
            guard let title = ProductType(rawValue: indexPath.row)?.title.lowercased() else {
                return
            }
            pushProductsViewController(with: title)
        default: break
        }
    }
}

extension HomeViewController: HomeTableViewCellDelegate {
    func handleHomeTableViewCellClicked(product: Product) {
        let builder = ProductBuilder()
        let viewController = builder.createViewController(with: product)
        navigationController?.pushViewController(viewController, animated: true)
    }
}
