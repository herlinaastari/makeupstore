//
//  ProductsViewController.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 23/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import UIKit

protocol ProductsViewControllerProtocol: class {
    func showProducts(_ products: [Product])
}

class ProductsViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    private let cellIdentifier = "ProductCollectionViewCell"
    var interactor: ProductsInteractorProtocol!
    var productType: String!
    var products: [Product]?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
        title = productType.uppercased()
        
        addCustomLeftBarBackButton()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        interactor.fetchProducts(by: productType)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    
    private func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
    }
    
    private func pushProductViewController(withProduct product: Product) {
        let builder = ProductBuilder()
        let viewController = builder.createViewController(with: product)
        navigationController?.pushViewController(viewController, animated: true)
    }
}

extension ProductsViewController: ProductsViewControllerProtocol {
    func showProducts(_ products: [Product]) {
        self.products = products
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
}

extension ProductsViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? ProductCollectionViewCell else {
            return UICollectionViewCell()
        }
        if let product = products?[indexPath.item] {
            cell.set(product)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (UIScreen.main.bounds.width - 24) / 2
        let height = width * (3/2)
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let product = products?[indexPath.item] else { return }
        pushProductViewController(withProduct: product)
    }
    
}
