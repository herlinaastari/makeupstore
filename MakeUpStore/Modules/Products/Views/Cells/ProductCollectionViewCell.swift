//
//  ProductCollectionViewCell.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 24/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var brandLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    override func prepareForReuse() {
        productImageView.image = nil
    }
    
    func set(_ product: Product) {
        nameLabel.text = product.name
        brandLabel.text = product.brand
        
        if let priceSign = product.priceSign,
            let price = product.price {
            priceLabel.text = priceSign + price
        }
        
        if let imageLink = product.imageLink {
            productImageView.loadImageUsingCache(withUrl: imageLink)
        }
    }
    
    func setupView() {
        containerView.layer.borderWidth = 1
        containerView.layer.borderColor = UIColor(red: 159/255, green: 159/255, blue: 159/255, alpha: 0.1).cgColor
        containerView.layer.cornerRadius = 8
        
        containerView.layer.shadowColor = UIColor(red: 159/255, green: 159/255, blue: 159/255, alpha: 0.5).cgColor
        containerView.layer.shadowRadius = 2
        containerView.layer.shadowOffset = .init(width: 0, height: 1)
        containerView.layer.shadowOpacity = 1
        containerView.layer.masksToBounds = false
    }

}
