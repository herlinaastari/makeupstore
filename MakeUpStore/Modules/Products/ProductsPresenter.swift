//
//  ProductsPresenter.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 23/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Foundation

protocol ProductsPresenterProtocol: class {
    func showProducts(_ products: [Product])
}

class ProductsPresenter: ProductsPresenterProtocol {
    weak var viewController: ProductsViewControllerProtocol?
    
    init(viewController: ProductsViewControllerProtocol) {
        self.viewController = viewController
    }
    
    func showProducts(_ products: [Product]) {
        viewController?.showProducts(products)
    }
}
