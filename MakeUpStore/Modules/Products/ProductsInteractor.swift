//
//  ProductsInteractor.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 23/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Foundation

protocol ProductsInteractorProtocol: class {
    func fetchProducts(by productType: String)
    func fetchLocalProducts(by productType: String)
}

class ProductsInteractor: ProductsInteractorProtocol {
    let presenter: ProductsPresenterProtocol
    let apiService: APIServiceProtocol
    
    init(presenter: ProductsPresenterProtocol, apiService: APIServiceProtocol) {
        self.presenter = presenter
        self.apiService = apiService
    }
    
    func fetchProducts(by productType: String) {
        let queryParameters: [String: String] = [
            "product_type": productType
        ]
        apiService.fetchProducts(from: .products, queryParameters: queryParameters) { (result: Result<[Product], APIServiceError>) in
            switch result {
            case .success(let products):
                let filteredProducts = products.filter { $0.price != "0.0" && $0.imageLink != nil }
                self.presenter.showProducts(filteredProducts)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func fetchLocalProducts(by productType: String) {
        let productType = productType.replacingOccurrences(of: " ", with: "_", options: .literal, range: nil)
        apiService.fetchLocalProducts(from: .products) { (result: Result<[Product], APIServiceError>) in
            switch result {
            case .success(let products):
                let filteredProducts = products.filter { $0.productType == productType }
                self.presenter.showProducts(filteredProducts)
            default: break
            }
        }
    }
}
