//
//  ProductsBuilder.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 23/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Foundation

class ProductsBuilder {
    
    func createViewController(with productType: String) -> ProductsViewController {
        let viewController = ProductsViewController()
        viewController.productType = productType
        let presenter = ProductsPresenter(viewController: viewController)
        let apiService = APIService.shared
        let interactor = ProductsInteractor(presenter: presenter, apiService: apiService)
        viewController.interactor = interactor
        return viewController
    }
    
}
