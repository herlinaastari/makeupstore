//
//  OrderSuccessViewController.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 25/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import UIKit

class OrderSuccessViewController: UIViewController {

    @IBOutlet weak var backToHomeButton: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addCustomLeftBarBackButton()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        backToHomeButton.layer.cornerRadius = 4
    }
    
    @IBAction func handleBackToHomeButtonTouchUpInside(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
}
