//
//  OrderSuccessBuilder.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 25/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Foundation

class OrderSuccessBuilder {
    
    func createViewController() -> OrderSuccessViewController {
        let viewController = OrderSuccessViewController()
        return viewController
    }
    
}
