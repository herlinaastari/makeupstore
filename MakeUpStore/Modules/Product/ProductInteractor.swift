//
//  ProductInteractor.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 25/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Foundation

protocol ProductInteractorProtocol: class {
    func setProductToCart(_ product: Product, quantity: String)
}

class ProductInteractor: ProductInteractorProtocol {
    let cartService: CartServiceProtocol
    
    init(cartService: CartServiceProtocol) {
        self.cartService = cartService
    }
    
    func setProductToCart(_ product: Product, quantity: String) {
        cartService.setDummyDeliveryDetail()
        cartService.setDummyDeliveryMethod()
        cartService.setDummyPaymentMethod()
        
        let cartProduct = CartProduct(product: product, quantity: Int(quantity) ?? 0)
        cartService.addProduct(cartProduct)
    }
}
