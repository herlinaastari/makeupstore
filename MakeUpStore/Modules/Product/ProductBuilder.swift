//
//  ProductBuilder.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 24/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Foundation

class ProductBuilder {
    func createViewController(with product: Product) -> ProductViewController {
        let viewController = ProductViewController()
        let cartService = CartService.shared
        let interactor = ProductInteractor(cartService: cartService)
        viewController.interactor = interactor
        viewController.product = product
        return viewController
    }
}
