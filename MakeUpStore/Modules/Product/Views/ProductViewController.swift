//
//  ProductViewController.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 24/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import UIKit

class ProductViewController: UIViewController {

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var brandLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var buyNowButton: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var productColorsStackView: UIStackView!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var addToCartButton: UIButton!
    
    var interactor: ProductInteractorProtocol!
    var product: Product!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
        title = product.name
        
        self.addCustomLeftBarBackButton()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        set(product)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
    }

    func set(_ product: Product) {
        nameLabel.text = product.name
        brandLabel.text = product.brand
        
        if let priceSign = product.priceSign,
            let price = product.price {
            priceLabel.text = priceSign + price
        }
        
        if let imageLink = product.imageLink {
            productImageView.loadImageUsingCache(withUrl: imageLink)
        }
        
        descriptionLabel.text = product.description
        
        buyNowButton.layer.cornerRadius = 4
        addToCartButton.layer.cornerRadius = 4
        
        stepper.setDecrementImage(stepper.decrementImage(for: .normal), for: .normal)
        stepper.setIncrementImage(stepper.incrementImage(for: .normal), for: .normal)
    }
    
    @IBAction func handleStepperViewValueChanged(_ sender: UIStepper) {
        let quantity = String(describing: Int(sender.value))
        quantityLabel.text = quantity
    }
    
    @IBAction func handleBuyNowButton(_ sender: UIButton) {
        interactor.setProductToCart(product, quantity: "1")
        pushCheckoutViewController()
    }
    
    @IBAction func handleAddToCartButton(_ sender: UIButton) {
        guard let quantity = quantityLabel.text, !quantity.isEmpty, quantity != "0" else { return }
        interactor.setProductToCart(product, quantity: quantity)
        pushCartViewController()
    }
    
    private func pushCheckoutViewController() {
        let builder = CheckoutBuilder()
        let cartProduct = CartProduct(product: product, quantity: 1)
        let viewController = builder.createViewController(with: [cartProduct])
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    private func pushCartViewController() {
        let builder = CartBuilder()
        let viewController = builder.createViewController()
        navigationController?.pushViewController(viewController, animated: true)
    }
}
