//
//  CheckoutInteractor.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 25/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Foundation

protocol CheckoutInteractorProtocol: class {
    func setData()
}

class CheckoutInteractor: CheckoutInteractorProtocol {
    private let presenter: CheckoutPresenterProtocol
    private let cartService: CartServiceProtocol
    private let cartProducts: [CartProduct]
    
    init(presenter: CheckoutPresenterProtocol, cartService: CartServiceProtocol, cartProducts: [CartProduct]) {
        self.presenter = presenter
        self.cartService = cartService
        self.cartProducts = cartProducts
    }
    
    func setData() {
        var cart = Cart()
        cart.deliveryDetail = cartService.getDeliveryDetails()?.first
        cart.deliveryMethod = cartService.getDeliveryMethods()?.first
        cart.paymentMethod = cartService.getPaymentMethods()?.first
        cart.products = cartProducts
        
        presenter.showData(cart)
    }
}
