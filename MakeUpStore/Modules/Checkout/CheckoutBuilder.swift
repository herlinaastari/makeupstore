//
//  CheckoutBuilderswift.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 24/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

class CheckoutBuilder {
    func createViewController(with cartProducts: [CartProduct]) -> CheckoutViewController {
        let viewController = CheckoutViewController()
        let presenter = CheckoutPresenter(viewController: viewController)
        let cartService = CartService.shared
        let interactor = CheckoutInteractor(presenter: presenter, cartService: cartService, cartProducts: cartProducts)
        viewController.interactor = interactor
        viewController.hidesBottomBarWhenPushed = true
        return viewController
    }
}
