//
//  CheckoutPresenter.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 25/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Foundation

protocol CheckoutPresenterProtocol: class {
    func showData(_ cart: Cart)
}

class CheckoutPresenter: CheckoutPresenterProtocol {
    weak var viewController: CheckoutViewControllerProtocol?
    
    init(viewController: CheckoutViewControllerProtocol) {
        self.viewController = viewController
    }
    
    func showData(_ cart: Cart) {
        viewController?.showData(cart)
        
        if let products = cart.products {
            showTotalQuantity(from: products)
            showTotalPrice(from: products)
        }
    }
    
    private func showTotalQuantity(from products: [CartProduct]) {
        let totalQuantity = products.reduce(0, { $0 + $1.quantity })
        viewController?.showTotalQuantity(String(describing: totalQuantity))

    }
    
    private func showTotalPrice(from products: [CartProduct]) {
        let totalPrice = products.reduce(0.0, { $0 + (($1.price) * Double($1.quantity)) })
        viewController?.showTotalPrice((products.first?.priceSign ?? "") + String(describing: totalPrice))
    }
}
