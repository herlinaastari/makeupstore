//
//  CheckoutProductTableViewCell.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 01/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import UIKit

protocol CheckoutProductTableViewCellDelegate: class {
    func handleSelectedButtonTouchUpInside(product: CartProduct, isSelected: Bool)
    func handleMinusButtonTouchUpInside(product: CartProduct, quantity: Int)
    func handlePlusButtonTouchUpInside(product: CartProduct, quantity: Int)
}

class CheckoutProductTableViewCell: UITableViewCell {

    @IBOutlet private weak var selectedButtonView: UIView!
    @IBOutlet weak var selectedButton: UIButton!
    @IBOutlet private weak var productImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var brandLabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var quantityLabel: UILabel!
    @IBOutlet private weak var actionButtonsView: UIView!
    @IBOutlet private weak var minusButton: UIButton!
    @IBOutlet private weak var updatedQuantityLabel: UILabel!
    @IBOutlet private weak var plusButton: UIButton!
    private var quantity = 0
    private var product: CartProduct!
    weak var delegate: CheckoutProductTableViewCellDelegate?
    
    func set(_ product: CartProduct) {
        self.product = product
        quantity = product.quantity
        
        if let imageLink = product.imageLink {
            productImageView.isHidden = false
            productImageView.loadImageUsingCache(withUrl: imageLink)
        } else {
            productImageView.isHidden = true
        }
        nameLabel.text = product.name
        brandLabel.text = product.brand
        let priceSign = product.priceSign ?? ""
        priceLabel.text = priceSign + String(describing: product.price)
        quantityLabel.text = "Qty: " + String(describing: product.quantity)
        updatedQuantityLabel.text = String(describing: product.quantity)
    }
    
    func hideActionButtonsView() {
        actionButtonsView.isHidden = true
    }
    
    func hideSelectedButton() {
        selectedButtonView.isHidden = true
    }
    
    @IBAction func handleSelectedButtonTouchUpInside(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        delegate?.handleSelectedButtonTouchUpInside(product: product, isSelected: sender.isSelected)
    }
    
    @IBAction func handleMinusButtonTouchUpInside(_ sender: UIButton) {
        quantity -= 1
        updatedQuantityLabel.text = "\(quantity)"
        delegate?.handleMinusButtonTouchUpInside(product: product, quantity: quantity)
    }
    
    @IBAction func handlePlusButtonTouchUpInside(_ sender: UIButton) {
        quantity += 1
        updatedQuantityLabel.text = "\(quantity)"
        delegate?.handlePlusButtonTouchUpInside(product: product, quantity: quantity)
    }
    
}
