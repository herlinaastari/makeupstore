//
//  CheckoutProductView.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 26/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import UIKit

protocol CheckoutProductViewDelegate: class {
    func setTableViewHeight(_ height: CGFloat)
}

class CheckoutProductView: CustomView {

    override var nibName: String { return "CheckoutProductView" }
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    private let cellIdentifier = "CheckoutProductTableViewCell"
    var products: [CartProduct]?
    weak var delegate: CheckoutProductViewDelegate?
    
    deinit {
        tableView.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func setupViews() {
        let separatorWidth = UIScreen.main.bounds.width - 32
        separatorView.createDottedLine(cgPoints: [CGPoint(x: 0, y: 0), CGPoint(x: separatorWidth, y: 0)])
        
        containerView.layer.borderColor = UIColor(red: 202/255, green: 85/255, blue: 101/255, alpha: 0.1).cgColor
        containerView.layer.borderWidth = 1
        containerView.layer.cornerRadius = 4
        
        setupTableView()
    }
    
    func set(_ products: [CartProduct]) {
        self.products = products
        tableView.reloadData()
        tableView.layoutIfNeeded()
        tableView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
    }
    
    func reloadTableView() {
        tableView.reloadData()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?){
        if(keyPath == "contentSize"){
            if let newvalue = change?[.newKey]{
                let newsize  = newvalue as! CGSize
                delegate?.setTableViewHeight(newsize.height)
            }
        }
    }
    
    private func setupTableView() {
        tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
    }
}

extension CheckoutProductView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! CheckoutProductTableViewCell
        if let product = products?[indexPath.row] {
            cell.set(product)
        }
        cell.hideSelectedButton()
        cell.hideActionButtonsView()
        cell.selectionStyle = .none
        cell.layoutIfNeeded()
        return cell
    }
}
