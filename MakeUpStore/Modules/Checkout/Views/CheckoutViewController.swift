//
//  CheckoutViewController.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 24/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import UIKit

protocol CheckoutViewControllerProtocol: class {
    func showData(_ cart: Cart)
    func showTotalPrice(_ totalPrice: String)
    func showTotalQuantity(_ totalQuantity: String)
}

class CheckoutViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var checkoutButton: UIButton!
    @IBOutlet weak var containerViewHeightConstraint: NSLayoutConstraint!

    var interactor: CheckoutInteractorProtocol!
    var containerHeight: CGFloat = 0.0
    var trayView: TrayContainerView?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "CHECKOUT"
        addCustomLeftBarBackButton()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkoutButton.layer.cornerRadius = 4
        interactor.setData()
    }
    
    private func pushOrderSuccessViewController() {
        let builder = OrderSuccessBuilder()
        let viewController = builder.createViewController()
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    private func addContainerViewHeightConstraint(with additionalHeight: CGFloat) {
        containerHeight += additionalHeight
        containerViewHeightConstraint.constant = containerHeight
    }
    
    private func addTrayView() {
        let trayView = TrayContainerView()
        trayView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(trayView)
       
        trayView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        trayView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        trayView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        trayView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        
        trayView.showModalViewWithAnimation()
    }
    
    @IBAction private func handleCheckoutButton(_ sender: Any) {
        pushOrderSuccessViewController()
    }
}

extension CheckoutViewController: CheckoutViewControllerProtocol {
    func showData(_ cart: Cart) {
        if let deliveryDetail = cart.deliveryDetail {
            let deliveryDetailView = DeliveryDetailView()
            deliveryDetailView.set(deliveryDetail)
            stackView.addArrangedSubview(deliveryDetailView)
        }
        
        if let deliveryMethod = cart.deliveryMethod {
            let deliveryMethodView = DeliveryMethodView()
            deliveryMethodView.set(deliveryMethod)
            deliveryMethodView.delegate = self
            stackView.addArrangedSubview(deliveryMethodView)
        }
        
        if let paymentMethod = cart.paymentMethod {
            let paymentMethodView = PaymentMethodView()
            paymentMethodView.set(paymentMethod)
            stackView.addArrangedSubview(paymentMethodView)
        }
        
        if let products = cart.products {
            let checkoutProductView = CheckoutProductView()
            checkoutProductView.delegate = self
            checkoutProductView.set(products)
            stackView.addArrangedSubview(checkoutProductView)
        }
        
        let stackViewEstimation = stackView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        addContainerViewHeightConstraint(with: stackViewEstimation)
    }
    
    func showTotalPrice(_ totalPrice: String) {
        priceLabel.text = totalPrice
    }
    
    func showTotalQuantity(_ totalQuantity: String) {
        quantityLabel.text = "(" + totalQuantity + " items" + ")"
    }
}

extension CheckoutViewController: CheckoutProductViewDelegate {
    func setTableViewHeight(_ height: CGFloat) {
        containerViewHeightConstraint.constant = containerHeight + height
    }
}

extension CheckoutViewController: DeliveryMethodViewDelegate {
    func handleChangeButtonDidTouchUpInside() {
        addTrayView()
    }
}
