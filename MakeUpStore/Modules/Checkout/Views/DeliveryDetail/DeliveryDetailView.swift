//
//  DeliveryDetailView.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 29/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import UIKit

class DeliveryDetailView: CustomView {
    
    override var nibName: String { return "DeliveryDetailView" }

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    override func setupViews() {
        setupContainerView()
        setupButton()
        setupSeparatorView()
    }
    
    func set(_ deliveryDetail: DeliveryDetail) {
        nameLabel.text = deliveryDetail.name
        phoneLabel.text = deliveryDetail.phone
        addressLabel.text = deliveryDetail.address
    }
    
    private func setupContainerView() {
        containerView.layer.borderColor = UIColor(red: 202/255, green: 85/255, blue: 101/255, alpha: 0.1).cgColor
        containerView.layer.borderWidth = 1
        containerView.layer.cornerRadius = 4
    }
    
    private func setupButton() {
        let icon = UIImage(named: "icon-pencil")!.withRenderingMode(.alwaysTemplate)
        editButton.setImage(icon, for: .normal)
        editButton.tintColor = UIColor(red: 202/255, green: 85/255, blue: 101/255, alpha: 0.9)
        editButton.imageView?.contentMode = .scaleAspectFit
        editButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: -20, bottom: 0, right: 0)
    }
    
    private func setupSeparatorView() {
        let separatorWidth = UIScreen.main.bounds.width - 32
        separatorView.createDottedLine(cgPoints: [CGPoint(x: 0, y: 0), CGPoint(x: separatorWidth, y: 0)])
    }    
}
