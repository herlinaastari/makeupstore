//
//  PaymentMethodView.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 01/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import UIKit

class PaymentMethodView: CustomView {

    override var nibName: String { return "PaymentMethodView" }

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var changeButton: UIButton!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func setupViews() {
        setupContainerView()
        setupButton()
        setupSeparatorView()
    }
    
    func set(_ paymentMethod: PaymentMethod) {
        nameLabel.text = paymentMethod.name
    }
    
    private func setupContainerView() {
        containerView.layer.borderColor = UIColor(red: 202/255, green: 85/255, blue: 101/255, alpha: 0.1).cgColor
        containerView.layer.borderWidth = 1
        containerView.layer.cornerRadius = 4
    }
    
    private func setupButton() {
        let icon = UIImage(named: "icon-pencil")!.withRenderingMode(.alwaysTemplate)
        changeButton.setImage(icon, for: .normal)
        changeButton.tintColor = UIColor(red: 202/255, green: 85/255, blue: 101/255, alpha: 0.9)
        changeButton.imageView?.contentMode = .scaleAspectFit
        changeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: -20, bottom: 0, right: 0)
    }
    
    private func setupSeparatorView() {
        let separatorWidth = UIScreen.main.bounds.width - 32
        separatorView.createDottedLine(cgPoints: [CGPoint(x: 0, y: 0), CGPoint(x: separatorWidth, y: 0)])
    }
    
}
