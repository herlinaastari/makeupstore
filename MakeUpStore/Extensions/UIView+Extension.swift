//
//  UIView+Extension.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 01/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import UIKit

extension UIView {
    func createDottedLine(cgPoints: [CGPoint]) {
      let caShapeLayer = CAShapeLayer()
      caShapeLayer.strokeColor = UIColor.lightGray.cgColor
      caShapeLayer.lineWidth = 1
      caShapeLayer.lineDashPattern = [1, 2]
      let cgPath = CGMutablePath()
      cgPath.addLines(between: cgPoints)
      caShapeLayer.path = cgPath
      layer.addSublayer(caShapeLayer)
   }
}
