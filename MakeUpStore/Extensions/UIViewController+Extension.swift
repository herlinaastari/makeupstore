//
//  UIViewController+Extension.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 26/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import UIKit

extension UIViewController {
    func addCustomLeftBarBackButton() {
        let image = UIImage(named: "icon-back")?.withRenderingMode(.alwaysOriginal)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style:.plain, target: self, action: #selector(popViewController))
    }
    
    @IBAction private func popViewController() {
        self.navigationController?.popViewController(animated: true)
    }
}
