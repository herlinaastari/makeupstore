//
//  Cart.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 01/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

struct Cart: Codable {
    var deliveryDetail: DeliveryDetail?
    var deliveryMethod: DeliveryMethod?
    var paymentMethod: PaymentMethod?
    var products: [CartProduct]?
}
