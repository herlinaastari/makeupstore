//
//  PaymentMethod.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 01/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

struct PaymentMethod: Codable {
    let id: Int
    let name: String
}
