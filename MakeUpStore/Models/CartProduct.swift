//
//  CartProduct.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 25/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

struct CartProduct: Codable {
    let id: Int
    let brand: String?
    let name: String
    let priceSign: String?
    let imageLink: String?
    var price: Double = 0
    var quantity: Int = 0
    
    init(product: Product, quantity: Int) {
        id = product.id
        brand = product.brand
        name = product.name
        priceSign = product.priceSign
        imageLink = product.imageLink
        if let price = Double(product.price ?? "") {
            self.price = price
        }
        self.quantity = quantity
    }
}
