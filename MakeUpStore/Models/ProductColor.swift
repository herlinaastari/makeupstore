//
//  ProductColor.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 24/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

struct ProductColor: Codable {
    let hexValue: String?
    let colorName: String?
    
    enum CodingKeys: String, CodingKey {
        case hexValue, colorName
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        hexValue = try values.decodeIfPresent(String.self, forKey: .hexValue)
        colorName = try values.decodeIfPresent(String.self, forKey: .colorName)
    }
}
