//
//  ProductType.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 23/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

enum ProductType: Int, CaseIterable {
    case blush
    case bronzer
    case eyebrow
    case eyeliner
    case eyeshadow
    case foundation
    case lipLiner
    case lipstick
    case mascara
    case nailPolish
    
    var title: String {
        switch self {
        case .blush:
            return "Blush"
        case .bronzer:
            return "Bronzer"
        case .eyebrow:
            return "Eyebrow"
        case .eyeliner:
            return "Eyeliner"
        case .eyeshadow:
            return "Eyeshadow"
        case .foundation:
            return "Foundation"
        case .lipLiner:
            return "Lip Liner"
        case .lipstick:
            return "Lipstick"
        case .mascara:
            return "Mascara"
        case .nailPolish:
            return "Nail Polish"
        }
    }
    
    var iconImageName: String {
        switch self {
        case .blush:
            return "icon-blush"
        case .bronzer:
            return "icon-bronzer"
        case .eyebrow:
            return "icon-eyebrow"
        case .eyeliner:
            return "icon-eyeliner"
        case .eyeshadow:
            return "icon-eyeshadow"
        case .foundation:
            return "icon-foundation"
        case .lipLiner:
            return "icon-lipliner"
        case .lipstick:
            return "icon-lipstick"
        case .mascara:
            return "icon-mascara"
        case .nailPolish:
            return "icon-nailpolish"
        }
    }
}
