//
//  DeliveryDetail.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 25/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

struct DeliveryDetail: Codable {
    var id: Int
    var name: String
    var phone: String
    var address: String
}
