//
//  Product.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 23/04/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

struct Product: Codable {
    let id: Int
    let brand: String?
    let name: String
    let price: String?
    let priceSign: String?
    let currency: String?
    let imageLink: String?
    let productLink: String?
    let description: String?
    let rating: Double?
    let category: String?
    let productType: String?
    let tagList: [String]?
    let createdAt: String?
    let updatedAt: String?
    let productApiUrl: String?
    let apiFeaturedImage: String?
    let productColors: [ProductColor]?
    
    enum CodingKeys: String, CodingKey {
        case id, brand, name, price, priceSign, currency, imageLink, productLink, description, rating, category, productType, tagList, createdAt, updatedAt, productApiUrl, apiFeaturedImage, productColors
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(Int.self, forKey: .id)
        brand = try values.decodeIfPresent(String.self, forKey: .brand)
        name = try values.decode(String.self, forKey: .name)
        price = try values.decodeIfPresent(String.self, forKey: .price)
        priceSign = try values.decodeIfPresent(String.self, forKey: .priceSign)
        currency = try values.decodeIfPresent(String.self, forKey: .currency)
        imageLink = try values.decodeIfPresent(String.self, forKey: .imageLink)
        productLink = try values.decodeIfPresent(String.self, forKey: .productLink)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        rating = try values.decodeIfPresent(Double.self, forKey: .rating)
        category = try values.decodeIfPresent(String.self, forKey: .category)
        productType = try values.decodeIfPresent(String.self, forKey: .productType)
        tagList = try values.decodeIfPresent([String].self, forKey: .tagList)
        createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt)
        updatedAt = try values.decodeIfPresent(String.self, forKey: .updatedAt)
        productApiUrl = try values.decodeIfPresent(String.self, forKey: .productApiUrl)
        apiFeaturedImage = try values.decodeIfPresent(String.self, forKey: .apiFeaturedImage)
        productColors = try values.decodeIfPresent([ProductColor].self, forKey: .productColors)
    }
}
