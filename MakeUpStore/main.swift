//
//  main.swift
//  MakeUpStore
//
//  Created by Herlina Astari on 20/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Foundation
import UIKit

let appDelegateClass: AnyClass = NSClassFromString("MockedAppDelegate") ?? AppDelegate.self

UIApplicationMain(CommandLine.argc, CommandLine.unsafeArgv, nil, NSStringFromClass(appDelegateClass))
