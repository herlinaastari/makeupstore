//
//  APIServiceTests.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 01/06/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Quick
import Nimble
@testable import MakeUpStore

class APIServiceTests: QuickSpec {
    var apiService: APIService!
    var mockedURLSession: MockedURLSession!
    
    override func spec() {
        super.spec()
        
        describe("Test API Service") {
            context("Fetch product successfully") {
                beforeEach {
                    self.setupAPIService()
                    
                    let url = URL(string: "http://makeup-api.herokuapp.com/api/v1")!
                    let httpURLResponse = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)!
                    let data = Data()
                    let apiResponse: (Result<(URLResponse, Data), Error>, Void) = (.success((httpURLResponse, data)), ())
                    self.mockedURLSession.stubbedDataTaskResultResult = apiResponse
                    self.mockedURLSession.stubbedDataTaskResult = URLSessionDataTask()
                    
                    waitUntil { done in
                        self.apiService.fetchProducts(from: .products) { result in
                            done()
                        }
                    }
                }
                
                it("Should fetch product") {
                    let invokedDataTask = self.mockedURLSession.invokedDataTask
                    expect(invokedDataTask).to(beTrue())
                }
            }
            
            context("Fetch product successfully with error code") {
                beforeEach {
                    self.setupAPIService()
                    
                    let urlResponse = URLResponse()
                    let data = Data()
                    let apiResponse: (Result<(URLResponse, Data), Error>, Void) = (.success((urlResponse, data)), ())
                    self.mockedURLSession.stubbedDataTaskResultResult = apiResponse
                    self.mockedURLSession.stubbedDataTaskResult = URLSessionDataTask()
                    
                    waitUntil { done in
                        self.apiService.fetchProducts(from: .products) { result in
                            done()
                        }
                    }
                }
                
                it("Should fetch product") {
                    let invokedDataTask = self.mockedURLSession.invokedDataTask
                    expect(invokedDataTask).to(beTrue())
                }
                
            }
            
            context("Fetch product with error") {
                beforeEach {
                    self.setupAPIService()
                    
                    let error = NSError()
                    let apiResponse: (Result<(URLResponse, Data), Error>, Void) = (.failure(error), ())
                    self.mockedURLSession.stubbedDataTaskResultResult = apiResponse
                    self.mockedURLSession.stubbedDataTaskResult = URLSessionDataTask()
                    
                    waitUntil { done in
                        self.apiService.fetchProducts(from: .products) { result in
                            done()
                        }
                    }
                }
                
                it("Should fetch product") {
                    let invokedDataTask = self.mockedURLSession.invokedDataTask
                    expect(invokedDataTask).to(beTrue())
                }
            }
            
            context("Fetch product successfully") {
                beforeEach {
                    self.setupAPIService()
                    
                    waitUntil { done in
                        self.apiService.fetchLocalProducts(from: .products) { result in
                            done()
                        }
                    }
                }
                it("Should fetch product") {
                    let invokedDataTask = self.mockedURLSession.invokedDataTask
                    expect(invokedDataTask).to(beFalse())
                }
            }
        }
    }
    
    private func setupAPIService() {
        mockedURLSession = MockedURLSession()
        apiService = APIService(urlSession: mockedURLSession)
    }
}
