//
//  MockProduct.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 25/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Foundation
@testable import MakeUpStore

extension Product {
    static var fake: Product {
        let product = [Product].fake.first!
        return product
    }
}

extension Sequence where Iterator.Element == Product {
    static var fake: [Product] {
        let product: [Product] = getJSONData()
        return product
    }
}

func getJSONData<T: Decodable>() -> [T] {
    let fileName = "makeup"
    let jsonDecoder = JSONDecoder()
    
    if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            let values = try jsonDecoder.decode([T].self, from: data)
            return values
        } catch {}
    }
    return []
}

func getJSONData<T: Decodable>() -> T? {
    let fileName = "makeup"
    let jsonDecoder = JSONDecoder()
    
    if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            let values = try jsonDecoder.decode(T.self, from: data)
            return values
        } catch {}
    }
    return nil
}
