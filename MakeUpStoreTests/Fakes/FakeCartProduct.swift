//
//  FakeCartProduct.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 31/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Foundation
@testable import MakeUpStore

extension CartProduct {
    static var fake: CartProduct {
        let product: Product = [Product].fake.first!
        let cartProduct: CartProduct = CartProduct(product: product, quantity: 1)
        return cartProduct
    }
}

extension Sequence where Iterator.Element == CartProduct {
    static var fake: [CartProduct] {
        let products: [Product] = getJSONData()
        let cartProducts = products.prefix(5).map {
            CartProduct(product: $0, quantity: 1)
        }
        return cartProducts
    }
}
