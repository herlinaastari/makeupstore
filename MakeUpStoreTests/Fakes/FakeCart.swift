//
//  FakeCart.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 01/06/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Foundation
@testable import MakeUpStore

extension Cart {
    static var fake: Cart {
        let products = [CartProduct].fake
        let cart = Cart(deliveryDetail: nil, deliveryMethod: nil, paymentMethod: nil, products: products)
        return cart
    }
}
