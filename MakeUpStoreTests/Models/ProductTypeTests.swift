//
//  ProductTypeTests.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 01/06/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Quick
import Nimble
@testable import MakeUpStore

class ProductTypeTests: QuickSpec {
    var model: ProductType!
    
    override func spec() {
        super.spec()
        
        describe("Test product type") {
            context("Create product type with raw value 0") {
                it("Should create title for blush") {
                    self.model = ProductType(rawValue: 0)
                    expect(self.model.title).to(match("Blush"))
                }
                
                it("Should create icon image for blush") {
                    self.model = ProductType(rawValue: 0)
                    expect(self.model.iconImageName).to(match("icon-blush"))
                }
            }
            
            context("Create product type with raw value 1") {
                it("Should create title for bronzer") {
                    self.model = ProductType(rawValue: 1)
                    expect(self.model.title).to(match("Bronzer"))
                }
                
                it("Should create icon image for blush") {
                    self.model = ProductType(rawValue: 1)
                    expect(self.model.iconImageName).to(match("icon-bronzer"))
                }
            }
            
            context("Create product type with raw value 2") {
                it("Should create title for eyebrow") {
                    self.model = ProductType(rawValue: 2)
                    expect(self.model.title).to(match("Eyebrow"))
                }
                
                it("Should create icon image for eyebrow") {
                    self.model = ProductType(rawValue: 2)
                    expect(self.model.iconImageName).to(match("icon-eyebrow"))
                }
            }
            
            context("Create product type with raw value 3") {
                it("Should create title for eyebrow") {
                    self.model = ProductType(rawValue: 3)
                    expect(self.model.title).to(match("Eyeliner"))
                }
                
                it("Should create icon image for eyebrow") {
                    self.model = ProductType(rawValue: 3)
                    expect(self.model.iconImageName).to(match("icon-eyeliner"))
                }
            }
            
            context("Create product type with raw value 4") {
                it("Should create title for eyebrow") {
                    self.model = ProductType(rawValue: 4)
                    expect(self.model.title).to(match("Eyeshadow"))
                }
                
                it("Should create icon image for eyebrow") {
                    self.model = ProductType(rawValue: 4)
                    expect(self.model.iconImageName).to(match("icon-eyeshadow"))
                }
            }
            
            context("Create product type with raw value 5") {
                it("Should create title for eyebrow") {
                    self.model = ProductType(rawValue: 5)
                    expect(self.model.title).to(match("Foundation"))
                }
                
                it("Should create icon image for eyebrow") {
                    self.model = ProductType(rawValue: 5)
                    expect(self.model.iconImageName).to(match("icon-foundation"))
                }
            }
            
            context("Create product type with raw value 6") {
                it("Should create title for eyebrow") {
                    self.model = ProductType(rawValue: 6)
                    expect(self.model.title).to(match("Lip Liner"))
                }
                
                it("Should create icon image for eyebrow") {
                    self.model = ProductType(rawValue: 6)
                    expect(self.model.iconImageName).to(match("icon-lipliner"))
                }
            }
            
            context("Create product type with raw value 7") {
                it("Should create title for eyebrow") {
                    self.model = ProductType(rawValue: 7)
                    expect(self.model.title).to(match("Lipstick"))
                }
                
                it("Should create icon image for eyebrow") {
                    self.model = ProductType(rawValue: 7)
                    expect(self.model.iconImageName).to(match("icon-lipstick"))
                }
            }
            
            context("Create product type with raw value 8") {
                it("Should create title for eyebrow") {
                    self.model = ProductType(rawValue: 8)
                    expect(self.model.title).to(match("Mascara"))
                }
                
                it("Should create icon image for eyebrow") {
                    self.model = ProductType(rawValue: 8)
                    expect(self.model.iconImageName).to(match("icon-mascara"))
                }
            }
            
            context("Create product type with raw value 9") {
                it("Should create title for eyebrow") {
                    self.model = ProductType(rawValue: 9)
                    expect(self.model.title).to(match("Nail Polish"))
                }
                
                it("Should create icon image for eyebrow") {
                    self.model = ProductType(rawValue: 9)
                    expect(self.model.iconImageName).to(match("icon-nailpolish"))
                }
            }
        }
    }
}
