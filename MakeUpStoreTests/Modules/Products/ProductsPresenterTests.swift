//
//  ProductsPresenterTests.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 30/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Quick
import Nimble
@testable import MakeUpStore

class ProductsPresenterTests: QuickSpec {
    var presenter: ProductsPresenter!
    var mockedViewController: MockedProductsViewController!
    
    override func spec() {
        super.spec()
        
        describe("Show products") {
            context("Has data") {
                beforeEach {
                    self.setupPresenter()
                    
                    let products = [Product].fake
                    self.presenter.showProducts(products)
                }
                
                it("Should show all products") {
                    let invokedShowProducts = self.mockedViewController.invokedShowProducts
                    expect(invokedShowProducts).to(beTrue())
                }
            }
        }
    }
    
    private func setupPresenter() {
        mockedViewController = MockedProductsViewController()
        presenter = ProductsPresenter(viewController: mockedViewController)
    }
}
