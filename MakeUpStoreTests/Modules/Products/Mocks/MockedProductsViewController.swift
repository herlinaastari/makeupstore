//
//  MockedProductsViewController.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 30/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

@testable import MakeUpStore

class MockedProductsViewController: ProductsViewControllerProtocol {
    var invokedShowProducts = false
    var invokedShowProductsCount = 0
    var invokedShowProductsParameters: (products: [Product], Void)?
    var invokedShowProductsParametersList = [(products: [Product], Void)]()
    func showProducts(_ products: [Product]) {
        invokedShowProducts = true
        invokedShowProductsCount += 1
        invokedShowProductsParameters = (products, ())
        invokedShowProductsParametersList.append((products, ()))
    }
}
