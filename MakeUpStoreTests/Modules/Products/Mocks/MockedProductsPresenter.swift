//
//  MockedProductsPresenter.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 28/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

@testable import MakeUpStore

class MockedProductsPresenter: ProductsPresenterProtocol {
    var invokedShowProducts = false
    var invokedShowProductsCount = 0
    var invokedShowProductsParameters: (products: [Product], Void)?
    var invokedShowProductsParametersList = [(products: [Product], Void)]()
    func showProducts(_ products: [Product]) {
        invokedShowProducts = true
        invokedShowProductsCount += 1
        invokedShowProductsParameters = (products, ())
        invokedShowProductsParametersList.append((products, ()))
    }
}
