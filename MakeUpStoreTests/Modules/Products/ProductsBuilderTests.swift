//
//  ProductsBuilderTests.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 31/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Quick
import Nimble
@testable import MakeUpStore

class ProductsBuilderTests: QuickSpec {
    var builder: ProductsBuilder!
    
    override func spec() {
        super.spec()
        
        builder = ProductsBuilder()
        
        describe("Test products builder") {
            context("Create view controller") {
                it("Should create view controller") {
                    let viewController = self.builder.createViewController(with: "product type")
                    expect(viewController).to(beAKindOf(ProductsViewController.self))
                }
            }
        }
    }
}
