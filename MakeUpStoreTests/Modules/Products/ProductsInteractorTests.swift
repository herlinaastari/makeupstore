//
//  ProductsInteractorTests.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 28/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Quick
import Nimble
@testable import MakeUpStore

class ProductsInteractorTests: QuickSpec {
    
    var interactor: ProductsInteractor!
    var mockedPresenter: MockedProductsPresenter!
    var mockedApiService: MockedApiService!
    
    override func spec() {
        super.spec()
        
        describe("Fetch products") {
            context("Has data") {
                beforeEach {
                    self.setupInteractor()
                    
                    let products = [Product].fake
                    let result: Result<[Product], APIServiceError> = .success(products)
                    self.mockedApiService.stubbedFetchProductsResultResult = (result, ())
                    self.interactor.fetchProducts(by: "lipstick")
                }
                
                it("Should show all products filtered by lipstick") {
                    let invokedShowProducts = self.mockedPresenter.invokedShowProducts
                    expect(invokedShowProducts).to(beTrue())
                }
            }
        }
        
        describe("Fetch products") {
            context("Has data") {
                beforeEach {
                    self.setupInteractor()
                    
                    let result: Result<[Product], APIServiceError> = .failure(.apiError)
                    self.mockedApiService.stubbedFetchProductsResultResult = (result, ())
                    self.interactor.fetchProducts(by: "lipstick")
                }

                it("Should show all products filtered by lipstick") {
                    let invokedShowProducts = self.mockedPresenter.invokedShowProducts
                    expect(invokedShowProducts).to(beFalse())
                }
            }
        }
        
        describe("Fetch local products") {
            context("Has local data") {
                beforeEach {
                    self.setupInteractor()
                    
                    let products = [Product].fake
                    let result: Result<[Product], APIServiceError> = .success(products)
                    self.mockedApiService.stubbedFetchLocalProductsResultResult = (result, ())
                    self.interactor.fetchLocalProducts(by: "lipstick")
                }
                
                it("Should show all products filtered by lipstick") {
                    let invokedShowProducts = self.mockedPresenter.invokedShowProducts
                    expect(invokedShowProducts).to(beTrue())
                }
                self.setupInteractor()
            }
        }
        
        describe("Fetch local products") {
            context("Has local data") {
                beforeEach {
                    self.setupInteractor()
                    
                    let result: Result<[Product], APIServiceError> = .failure(.apiError)
                    self.mockedApiService.stubbedFetchLocalProductsResultResult = (result, ())
                    self.interactor.fetchLocalProducts(by: "lipstick")
                }
                
                it("Should show all products filtered by lipstick") {
                    let invokedShowProducts = self.mockedPresenter.invokedShowProducts
                    expect(invokedShowProducts).to(beFalse())
                }
                self.setupInteractor()
            }
        }
    }
    
    private func setupInteractor() {
        self.mockedPresenter = MockedProductsPresenter()
        self.mockedApiService = MockedApiService()
        self.interactor = ProductsInteractor(presenter: self.mockedPresenter, apiService: self.mockedApiService)
    }
}
