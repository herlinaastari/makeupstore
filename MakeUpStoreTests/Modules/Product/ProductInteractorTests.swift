//
//  ProductInteractorTests.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 30/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Quick
import Nimble
@testable import MakeUpStore

class ProductInteractorTests: QuickSpec {
    var interactor: ProductInteractor!
    var mockedCartService: MockedCartService!
    
    override func spec() {
        super.spec()
        
        describe("Set product to cart") {
            context("Has data") {
                beforeEach {
                    self.setupInteractor()
                    let product = [Product].fake.first!
                    self.interactor.setProductToCart(product, quantity: "1")
                }
                
                it("Should set product to cart") {
                    let invokedAddProduct = self.mockedCartService.invokedAddProduct
                    expect(invokedAddProduct).to(beTrue())
                }
            }
            
            context("Has incorrect quantity") {
                beforeEach {
                    self.setupInteractor()
                    let product = [Product].fake.first!
                    self.interactor.setProductToCart(product, quantity: "")
                }
                
                it("Should set product to cart") {
                    let invokedAddProduct = self.mockedCartService.invokedAddProduct
                    expect(invokedAddProduct).to(beTrue())
                }
            }
        }
    }
    
    private func setupInteractor() {
        mockedCartService = MockedCartService()
        interactor = ProductInteractor(cartService: mockedCartService)
    }
}
