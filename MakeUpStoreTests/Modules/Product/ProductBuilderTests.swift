//
//  ProductBuilderTests.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 31/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Quick
import Nimble
@testable import MakeUpStore

class ProductBuilderTests: QuickSpec {
    var builder: ProductBuilder!
    
    override func spec() {
        super.spec()
        
        builder = ProductBuilder()
        
        describe("Test product builder") {
            context("Create view controller") {
                it("Should create view controller") {
                    let product = Product.fake
                    let viewController = self.builder.createViewController(with: product)
                    expect(viewController).to(beAKindOf(ProductViewController.self))
                }
            }
        }
    }
}
