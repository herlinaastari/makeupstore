//
//  OrderSuccessBuilderTests.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 01/06/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Quick
import Nimble
@testable import MakeUpStore

class OrderSuccessBuilderTests: QuickSpec {
    var builder: OrderSuccessBuilder!
    
    override func spec() {
        super.spec()
        
        builder = OrderSuccessBuilder()
        
        describe("Test order success builder") {
            context("Create view controller") {
                it("Should create view controller") {
                    let viewController = self.builder.createViewController()
                    expect(viewController).to(beAKindOf(OrderSuccessViewController.self))
                }
            }
        }
    }
}
