//
//  HomePresenterTests.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 25/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Quick
import Nimble
@testable import MakeUpStore

class HomePresenterTests: QuickSpec {
    var presenter: HomePresenter!
    var mockedViewController: MockedHomeViewController!

    override func spec() {
        super.spec()
    
        describe("Fetch local products") {
            mockedViewController = MockedHomeViewController()
            presenter = HomePresenter(viewController: mockedViewController)
            
            context("Show best seller product") {
                let fakeProduct = [Product].fake
                self.presenter.showBestSellerProducts(fakeProduct)
                
                it("Should show new arrival products") {
                    let invokedShowNewArrivalProducts = self.mockedViewController.invokedShowNewArrivalProducts
                    expect(invokedShowNewArrivalProducts).to(beTrue())
                }
            }
            
            context("Show recommended products") {
                let fakeProduct = [Product].fake
                self.presenter.showRecommendedProducts(fakeProduct)
                
                it("Should show new arrival products") {
                    let invokedShowBestSellersProducts = self.mockedViewController.invokedShowBestSellersProducts
                    expect(invokedShowBestSellersProducts).to(beTrue())
                }
            }
        }
    }
}
