//
//  HomeInteractorTests.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 20/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Quick
import Nimble
@testable import MakeUpStore

class HomeInteractorTests: QuickSpec {
    var interactor: HomeInteractor!
    var mockedPresenter: MockedHomePresenter!
    var mockedApiService: MockedApiService!

    override func spec() {
        super.spec()

        mockedPresenter = MockedHomePresenter()
        mockedApiService = MockedApiService()
        interactor = HomeInteractor(presenter: mockedPresenter, apiService: mockedApiService)
        
        describe("Fetch local products") {
            context("Has data") {
                let products = [Product].fake
                let result: Result<[Product], APIServiceError> = .success(products)
                self.mockedApiService.stubbedFetchLocalProductsResultResult = (result, ())
                self.interactor.fetchLocalProducts()
                
                it("Should fetch local products") {
                    let invokedFetchLocalProducts = self.mockedApiService.invokedFetchLocalProducts
                    expect(invokedFetchLocalProducts).to(beTrue())
                }
                
                it("Should show best seller product") {
                    let invokedBestSellerProducts = self.mockedPresenter.invokedShowBestSellerProducts
                    expect(invokedBestSellerProducts).to(beTrue())
                }

                it("Should show best seller product") {
                    let invokedShowRecommendedProducts = self.mockedPresenter.invokedShowRecommendedProducts
                    expect(invokedShowRecommendedProducts).to(beTrue())
                }
            }
        }
    }
}

