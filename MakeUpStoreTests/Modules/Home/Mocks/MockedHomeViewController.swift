//
//  MockedHomeViewController.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 25/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

@testable import MakeUpStore

class MockedHomeViewController: HomeViewControllerProtocol {
    var invokedShowNewArrivalProducts = false
    var invokedShowNewArrivalProductsCount = 0
    var invokedShowNewArrivalProductsParameters: (products: [Product], Void)?
    var invokedShowNewArrivalProductsParametersList = [(products: [Product], Void)]()
    func showNewArrivalProducts(_ products: [Product]) {
        invokedShowNewArrivalProducts = true
        invokedShowNewArrivalProductsCount += 1
        invokedShowNewArrivalProductsParameters = (products, ())
        invokedShowNewArrivalProductsParametersList.append((products, ()))
    }
    var invokedShowBestSellersProducts = false
    var invokedShowBestSellersProductsCount = 0
    var invokedShowBestSellersProductsParameters: (products: [Product], Void)?
    var invokedShowBestSellersProductsParametersList = [(products: [Product], Void)]()
    func showBestSellersProducts(_ products: [Product]) {
        invokedShowBestSellersProducts = true
        invokedShowBestSellersProductsCount += 1
        invokedShowBestSellersProductsParameters = (products, ())
        invokedShowBestSellersProductsParametersList.append((products, ()))
    }
}
