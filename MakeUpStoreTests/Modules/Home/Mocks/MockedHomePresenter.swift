//
//  MockedHomePresenter.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 20/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

@testable import MakeUpStore

class MockedHomePresenter: HomePresenterProtocol {
    var invokedShowBestSellerProducts = false
    var invokedShowBestSellerProductsCount = 0
    var invokedShowBestSellerProductsParameters: (products: [Product], Void)?
    var invokedShowBestSellerProductsParametersList = [(products: [Product], Void)]()
    func showBestSellerProducts(_ products: [Product]) {
        invokedShowBestSellerProducts = true
        invokedShowBestSellerProductsCount += 1
        invokedShowBestSellerProductsParameters = (products, ())
        invokedShowBestSellerProductsParametersList.append((products, ()))
    }
    var invokedShowRecommendedProducts = false
    var invokedShowRecommendedProductsCount = 0
    var invokedShowRecommendedProductsParameters: (products: [Product], Void)?
    var invokedShowRecommendedProductsParametersList = [(products: [Product], Void)]()
    func showRecommendedProducts(_ products: [Product]) {
        invokedShowRecommendedProducts = true
        invokedShowRecommendedProductsCount += 1
        invokedShowRecommendedProductsParameters = (products, ())
        invokedShowRecommendedProductsParametersList.append((products, ()))
    }
}
