//
//  HomeBuilderTests.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 31/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Quick
import Nimble
@testable import MakeUpStore

class HomeBuilderTests: QuickSpec {
    var builder: HomeBuilder!
    
    override func spec() {
        super.spec()
        
        builder = HomeBuilder()
        
        describe("Test home builder") {
            context("Create view controller") {
                it("Should create view controller") {
                    let viewController = self.builder.createViewController()
                    expect(viewController).to(beAKindOf(HomeViewController.self))
                }
            }
        }
    }
}
