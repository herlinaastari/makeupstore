//
//  MockedCartPresenter.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 30/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

@testable import MakeUpStore

class MockedCartPresenter: CartPresenterProtocol {
    var invokedShowData = false
    var invokedShowDataCount = 0
    var invokedShowDataParameters: (cartProducts: [CartProduct], Void)?
    var invokedShowDataParametersList = [(cartProducts: [CartProduct], Void)]()
    func showData(_ cartProducts: [CartProduct]) {
        invokedShowData = true
        invokedShowDataCount += 1
        invokedShowDataParameters = (cartProducts, ())
        invokedShowDataParametersList.append((cartProducts, ()))
    }
    var invokedDeleteRow = false
    var invokedDeleteRowCount = 0
    var invokedDeleteRowParameters: (index: Int, Void)?
    var invokedDeleteRowParametersList = [(index: Int, Void)]()
    func deleteRow(_ index: Int) {
        invokedDeleteRow = true
        invokedDeleteRowCount += 1
        invokedDeleteRowParameters = (index, ())
        invokedDeleteRowParametersList.append((index, ()))
    }
}
