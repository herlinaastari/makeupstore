//
//  CartInteractorTests.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 30/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Quick
import Nimble
@testable import MakeUpStore

class CartInteractorTests: QuickSpec {
    var interactor: CartInteractorProtocol!
    var mockedPresenter: MockedCartPresenter!
    var mockedCartService: MockedCartService!
    
    override func spec() {
        super.spec()
        
        describe("Set data") {
            context("Has no data") {
                beforeEach {
                    self.setupInteractor()
                    self.interactor.setData()
                    
                }
                
                it("Should not show data") {
                    let invokedShowData = self.mockedPresenter.invokedShowData
                    expect(invokedShowData).to(beFalse())
                }
            }
            
            context("Has data") {
                beforeEach {
                    self.setupInteractor()
                    self.mockedCartService.stubbedGetProductsResult = [CartProduct].fake
                    self.interactor.setData()
                    
                }
                
                it("Should show data") {
                    let invokedShowData = self.mockedPresenter.invokedShowData
                    expect(invokedShowData).to(beTrue())
                }
            }
        }
        
        describe("Set product to cart") {
            context("Has no data") {
                beforeEach {
                    self.setupInteractor()
                    let cartProduct = CartProduct.fake
                    self.interactor.updateProductQuantity(2, to: cartProduct)
                }
                
                it("Should try to update product quantity in cart") {
                    let invokedUpdateProductQuantity = self.mockedCartService.invokedUpdateProductQuantity
                    expect(invokedUpdateProductQuantity).to(beTrue())
                }
                
                it("Should not show data") {
                    let invokedShowData = self.mockedPresenter.invokedShowData
                    expect(invokedShowData).to(beFalse())
                }
            }
            
            context("Has data") {
                beforeEach {
                    self.setupInteractor()
                    let cartProduct = CartProduct.fake
                    self.mockedCartService.stubbedGetProductsResult = [CartProduct].fake
                    self.interactor.updateProductQuantity(2, to: cartProduct)
                }
                
                it("Should update product quantity in cart") {
                    let invokedUpdateProductQuantity = self.mockedCartService.invokedUpdateProductQuantity
                    expect(invokedUpdateProductQuantity).to(beTrue())
                }
                
                it("Should show data") {
                    let invokedShowData = self.mockedPresenter.invokedShowData
                    expect(invokedShowData).to(beTrue())
                }
            }
        }
        
        describe("Delete product") {
            context("Has no data") {
                beforeEach {
                    self.setupInteractor()
                    self.interactor.deleteProduct(onIndex: 1)
                }
                
                it("Should not update product quantity in cart") {
                    let invokedDeleteProduct = self.mockedCartService.invokedDeleteProduct
                    expect(invokedDeleteProduct).to(beFalse())
                }
                
                it("Should not show data") {
                    let invokedDeleteRow = self.mockedPresenter.invokedDeleteRow
                    expect(invokedDeleteRow).to(beFalse())
                }
            }
            
            context("Has data") {
                beforeEach {
                    self.setupInteractor()
                    self.mockedCartService.stubbedGetProductsResult = [CartProduct].fake
                    self.interactor.deleteProduct(onIndex: 1)
                }
                
                it("Should update product quantity in cart") {
                    let invokedDeleteProduct = self.mockedCartService.invokedDeleteProduct
                    expect(invokedDeleteProduct).to(beTrue())
                }
                
                it("Should show data") {
                    let invokedDeleteRow = self.mockedPresenter.invokedDeleteRow
                    expect(invokedDeleteRow).to(beTrue())
                }
            }
        }
    }
    
    private func setupInteractor() {
        mockedPresenter = MockedCartPresenter()
        mockedCartService = MockedCartService()
        interactor = CartInteractor(presenter: mockedPresenter, cartService: mockedCartService)
    }
}
