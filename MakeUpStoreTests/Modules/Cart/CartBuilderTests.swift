//
//  CartBuilderTests.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 31/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Quick
import Nimble
@testable import MakeUpStore

class CartBuilderTests: QuickSpec {
    var builder: CartBuilder!
    
    override func spec() {
        super.spec()
        
        builder = CartBuilder()
        
        describe("Test cart builder") {
            context("Create view controller") {
                it("Should create view controller") {
                    let viewController = self.builder.createViewController()
                    expect(viewController).to(beAKindOf(CartViewController.self))
                }
            }
        }
    }
}
