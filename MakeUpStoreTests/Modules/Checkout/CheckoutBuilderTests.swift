//
//  CheckoutBuilderTests.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 31/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Quick
import Nimble
@testable import MakeUpStore

class CheckoutBuilderTests: QuickSpec {
    var builder: CheckoutBuilder!
    
    override func spec() {
        super.spec()
        
        builder = CheckoutBuilder()
        
        describe("Test checkout builder") {
            context("Create view controller") {
                it("Should create view controller") {
                    let cartProducts = [CartProduct].fake
                    let viewController = self.builder.createViewController(with: cartProducts)
                    expect(viewController).to(beAKindOf(CheckoutViewController.self))
                }
            }
        }
    }
}
