//
//  CheckoutPresenterTests.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 31/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Quick
import Nimble
@testable import MakeUpStore

class CheckoutPresenterTests: QuickSpec {
    var presenter: CheckoutPresenter!
    var mockedViewController: MockedCheckoutViewController!
    
    override func spec() {
        super.spec()
        
        describe("Test checkout presenter") {
            context("Show data") {
                beforeEach {
                    self.setupPresenter()
                    let cart = Cart.fake
                    self.presenter.showData(cart)
                }
                
                it("Should show data") {
                    let invokedShowData = self.mockedViewController.invokedShowData
                    expect(invokedShowData).to(beTrue())
                }
            }
        }
    }
    
    private func setupPresenter() {
        mockedViewController = MockedCheckoutViewController()
        presenter = CheckoutPresenter(viewController: mockedViewController)
    }
}
