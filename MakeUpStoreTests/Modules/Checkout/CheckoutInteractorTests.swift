//
//  CheckoutInteractorTests.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 31/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Quick
import Nimble
@testable import MakeUpStore

class CheckoutInteractorTests: QuickSpec {
    var interactor: CheckoutInteractor!
    var mockedPresenter: MockedCheckoutPresenter!
    var mockedCartService: MockedCartService!
    
    override func spec() {
        super.spec()
        
        describe("Test checkout interactor") {
            context("Set data") {
                beforeEach {
                    self.setupInteractor()
                    self.interactor.setData()
                }
            
                it("Should show data") {
                    let invokedShowData = self.mockedPresenter.invokedShowData
                    expect(invokedShowData).to(beTrue())
                }
            }
        }
    }
    
    private func setupInteractor() {
        mockedPresenter = MockedCheckoutPresenter()
        mockedCartService = MockedCartService()
        
        let cartProducts = [CartProduct].fake
        interactor = CheckoutInteractor(presenter: mockedPresenter, cartService: mockedCartService, cartProducts: cartProducts)
    }
}
