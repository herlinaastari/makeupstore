//
//  MockedCheckoutPresenter.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 31/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

@testable import MakeUpStore

class MockedCheckoutPresenter: CheckoutPresenterProtocol {
    var invokedShowData = false
    var invokedShowDataCount = 0
    var invokedShowDataParameters: (cart: Cart, Void)?
    var invokedShowDataParametersList = [(cart: Cart, Void)]()
    func showData(_ cart: Cart) {
        invokedShowData = true
        invokedShowDataCount += 1
        invokedShowDataParameters = (cart, ())
        invokedShowDataParametersList.append((cart, ()))
    }
}
