//
//  MockedCheckoutViewController.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 31/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

@testable import MakeUpStore

class MockedCheckoutViewController: CheckoutViewControllerProtocol {
    var invokedShowData = false
    var invokedShowDataCount = 0
    var invokedShowDataParameters: (cart: Cart, Void)?
    var invokedShowDataParametersList = [(cart: Cart, Void)]()
    func showData(_ cart: Cart) {
        invokedShowData = true
        invokedShowDataCount += 1
        invokedShowDataParameters = (cart, ())
        invokedShowDataParametersList.append((cart, ()))
    }
    var invokedShowTotalPrice = false
    var invokedShowTotalPriceCount = 0
    var invokedShowTotalPriceParameters: (totalPrice: String, Void)?
    var invokedShowTotalPriceParametersList = [(totalPrice: String, Void)]()
    func showTotalPrice(_ totalPrice: String) {
        invokedShowTotalPrice = true
        invokedShowTotalPriceCount += 1
        invokedShowTotalPriceParameters = (totalPrice, ())
        invokedShowTotalPriceParametersList.append((totalPrice, ()))
    }
    var invokedShowTotalQuantity = false
    var invokedShowTotalQuantityCount = 0
    var invokedShowTotalQuantityParameters: (totalQuantity: String, Void)?
    var invokedShowTotalQuantityParametersList = [(totalQuantity: String, Void)]()
    func showTotalQuantity(_ totalQuantity: String) {
        invokedShowTotalQuantity = true
        invokedShowTotalQuantityCount += 1
        invokedShowTotalQuantityParameters = (totalQuantity, ())
        invokedShowTotalQuantityParametersList.append((totalQuantity, ()))
    }
}
