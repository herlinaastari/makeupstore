//
//  CartServiceTests.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 01/06/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Quick
import Nimble
@testable import MakeUpStore

class CartServiceTests: QuickSpec {
    var cartService: CartService!
    var mockedUserDefaults: MockedUserDefaults!
    
    override func spec() {
        super.spec()
        
        describe("Test cart service") {
            context("Add product") {
                beforeEach {
                    self.setupCartService()
                    
                    let cartProduct = CartProduct.fake
                    self.cartService.addProduct(cartProduct)
                }
                
                it("Should add product") {
                    let invokedData = self.mockedUserDefaults.invokedData
                    expect(invokedData).to(beTrue())
                }
            }
            
            context("Update product") {
                beforeEach {
                    self.setupCartService()
                    
                    let cartProduct = CartProduct.fake
                    self.cartService.updateProductQuantity(2, to: cartProduct)
                }
                
                it("Should update product") {
                    let invokedData = self.mockedUserDefaults.invokedData
                    expect(invokedData).to(beTrue())
                }
            }
            
            context("Delete product") {
                beforeEach {
                    self.setupCartService()
                    
                    let cartProduct = CartProduct.fake
                    self.cartService.deleteProduct(cartProduct)
                }
                
                it("Should delete product") {
                    let invokedData = self.mockedUserDefaults.invokedData
                    expect(invokedData).to(beTrue())
                }
            }
        }
    }
    
    private func setupCartService() {
        mockedUserDefaults = MockedUserDefaults()
        cartService = CartService(userDefaults: mockedUserDefaults)
    }
}
