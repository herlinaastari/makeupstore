//
//  MockedURLSession.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 01/06/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Foundation
@testable import MakeUpStore

class MockedURLSession: URLSessionProtocol {
    var invokedDataTask = false
    var invokedDataTaskCount = 0
    var invokedDataTaskParameters: (url: URL, Void)?
    var invokedDataTaskParametersList = [(url: URL, Void)]()
    var stubbedDataTaskResultResult: (Result<(URLResponse, Data), Error>, Void)?
    var stubbedDataTaskResult: URLSessionDataTask!
    func dataTask(with url: URL, result: @escaping (Result<(URLResponse, Data), Error>) -> Void) -> URLSessionDataTask {
        invokedDataTask = true
        invokedDataTaskCount += 1
        invokedDataTaskParameters = (url, ())
        invokedDataTaskParametersList.append((url, ()))
        if let success = stubbedDataTaskResultResult {
            result(success.0)
        }
        return stubbedDataTaskResult
    }
}
