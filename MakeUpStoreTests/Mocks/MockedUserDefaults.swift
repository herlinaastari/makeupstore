//
//  MockedUserDefaults.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 01/06/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import Foundation
@testable import MakeUpStore

class MockedUserDefaults: UserDefaults {
    var invokedData = false
    var invokedDataCount = 0
    var invokedDataParameters: String?
    var invokedDataParametersList = [String]()
    override func data(forKey defaultName: String) -> Data? {
        invokedData = true
        invokedDataCount += 1
        invokedDataParameters = defaultName
        invokedDataParametersList.append(defaultName)
        return Data()
    }
}

