//
//  MockedCartService.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 30/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

@testable import MakeUpStore

class MockedCartService: CartServiceProtocol {
    var invokedGetCart = false
    var invokedGetCartCount = 0
    var stubbedGetCartResult: Cart!
    func getCart() -> Cart? {
        invokedGetCart = true
        invokedGetCartCount += 1
        return stubbedGetCartResult
    }
    var invokedGetDeliveryDetails = false
    var invokedGetDeliveryDetailsCount = 0
    var stubbedGetDeliveryDetailsResult: [DeliveryDetail]!
    func getDeliveryDetails() -> [DeliveryDetail]? {
        invokedGetDeliveryDetails = true
        invokedGetDeliveryDetailsCount += 1
        return stubbedGetDeliveryDetailsResult
    }
    var invokedGetDeliveryMethods = false
    var invokedGetDeliveryMethodsCount = 0
    var stubbedGetDeliveryMethodsResult: [DeliveryMethod]!
    func getDeliveryMethods() -> [DeliveryMethod]? {
        invokedGetDeliveryMethods = true
        invokedGetDeliveryMethodsCount += 1
        return stubbedGetDeliveryMethodsResult
    }
    var invokedGetPaymentMethods = false
    var invokedGetPaymentMethodsCount = 0
    var stubbedGetPaymentMethodsResult: [PaymentMethod]!
    func getPaymentMethods() -> [PaymentMethod]? {
        invokedGetPaymentMethods = true
        invokedGetPaymentMethodsCount += 1
        return stubbedGetPaymentMethodsResult
    }
    var invokedGetProducts = false
    var invokedGetProductsCount = 0
    var stubbedGetProductsResult: [CartProduct]!
    func getProducts() -> [CartProduct]? {
        invokedGetProducts = true
        invokedGetProductsCount += 1
        return stubbedGetProductsResult
    }
    var invokedAddProduct = false
    var invokedAddProductCount = 0
    var invokedAddProductParameters: (product: CartProduct, Void)?
    var invokedAddProductParametersList = [(product: CartProduct, Void)]()
    func addProduct(_ product: CartProduct) {
        invokedAddProduct = true
        invokedAddProductCount += 1
        invokedAddProductParameters = (product, ())
        invokedAddProductParametersList.append((product, ()))
    }
    var invokedUpdateProductQuantity = false
    var invokedUpdateProductQuantityCount = 0
    var invokedUpdateProductQuantityParameters: (quantity: Int, product: CartProduct)?
    var invokedUpdateProductQuantityParametersList = [(quantity: Int, product: CartProduct)]()
    func updateProductQuantity(_ quantity: Int, to product: CartProduct) {
        invokedUpdateProductQuantity = true
        invokedUpdateProductQuantityCount += 1
        invokedUpdateProductQuantityParameters = (quantity, product)
        invokedUpdateProductQuantityParametersList.append((quantity, product))
    }
    var invokedDeleteProduct = false
    var invokedDeleteProductCount = 0
    var invokedDeleteProductParameters: (product: CartProduct, Void)?
    var invokedDeleteProductParametersList = [(product: CartProduct, Void)]()
    func deleteProduct(_ product: CartProduct) {
        invokedDeleteProduct = true
        invokedDeleteProductCount += 1
        invokedDeleteProductParameters = (product, ())
        invokedDeleteProductParametersList.append((product, ()))
    }
    var invokedSetDummyDeliveryDetail = false
    var invokedSetDummyDeliveryDetailCount = 0
    func setDummyDeliveryDetail() {
        invokedSetDummyDeliveryDetail = true
        invokedSetDummyDeliveryDetailCount += 1
    }
    var invokedSetDummyPaymentMethod = false
    var invokedSetDummyPaymentMethodCount = 0
    func setDummyPaymentMethod() {
        invokedSetDummyPaymentMethod = true
        invokedSetDummyPaymentMethodCount += 1
    }
    var invokedSetDummyDeliveryMethod = false
    var invokedSetDummyDeliveryMethodCount = 0
    func setDummyDeliveryMethod() {
        invokedSetDummyDeliveryMethod = true
        invokedSetDummyDeliveryMethodCount += 1
    }
}
