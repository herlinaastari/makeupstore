//
//  MockedSceneDelegate.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 20/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

import UIKit

class MockedSceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    @available(iOS 13.0, *)
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }

        window = UIWindow(windowScene: windowScene)
        window?.rootViewController = UIViewController()
        window?.makeKeyAndVisible()
    }
}
