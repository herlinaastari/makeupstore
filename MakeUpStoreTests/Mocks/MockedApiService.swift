//
//  MockedApiService.swift
//  MakeUpStoreTests
//
//  Created by Herlina Astari on 29/05/20.
//  Copyright © 2020 Herlina Astari. All rights reserved.
//

@testable import MakeUpStore

class MockedApiService: APIServiceProtocol {
    var invokedFetchProducts = false
    var invokedFetchProductsCount = 0
    var invokedFetchProductsParameters: (endpoint: Endpoint, queryParameters: [String: String]?)?
    var invokedFetchProductsParametersList = [(endpoint: Endpoint, queryParameters: [String: String]?)]()
    var stubbedFetchProductsResultResult: (Result<[Product], APIServiceError>, (Void))?
    func fetchProducts(
    from endpoint: Endpoint,
    queryParameters: [String: String]?,
    result: @escaping (Result<[Product], APIServiceError>)
    -> Void
    ) {
        invokedFetchProducts = true
        invokedFetchProductsCount += 1
        invokedFetchProductsParameters = (endpoint, queryParameters)
        invokedFetchProductsParametersList.append((endpoint, queryParameters))
        if let success = stubbedFetchProductsResultResult {
            result(success.0)
        }
    }
    var invokedFetchLocalProducts = false
    var invokedFetchLocalProductsCount = 0
    var invokedFetchLocalProductsParameters: (endpoint: Endpoint, Void)?
    var invokedFetchLocalProductsParametersList = [(endpoint: Endpoint, Void)]()
    var stubbedFetchLocalProductsResultResult: (Result<[Product], APIServiceError>, Void)?
    func fetchLocalProducts(
    from endpoint: Endpoint,
    result: @escaping (Result<[Product], APIServiceError>)
    -> Void
    ) {
        invokedFetchLocalProducts = true
        invokedFetchLocalProductsCount += 1
        invokedFetchLocalProductsParameters = (endpoint, ())
        invokedFetchLocalProductsParametersList.append((endpoint, ()))
        if let success = stubbedFetchLocalProductsResultResult {
            result(success.0)
        }
    }
}
